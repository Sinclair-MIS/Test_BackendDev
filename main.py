import matplotlib.pyplot as plt
import matplotlib.image as img


class Point:
    X: int
    Y: int

    def __init__(self, x, y):
        self.X = x
        self.Y = y


image = img.imread('Resources/Cat.png')
startPoints = [Point(50, 50), Point(306, 50), Point(562, 50), Point(50, 306), Point(306, 306), Point(562, 306)]

for point in startPoints:
    image[point.Y][point.X] = [1, 0, 0, 1]

plt.imshow(image)
plt.show()
